<%@page import="entities.Item"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="util.Const"%>
<%@ page import="util.Misc"%>


<%
@SuppressWarnings("unchecked")

String msg =(String)request.getAttribute("msg");

Item item = (Item)request.getAttribute("produit_unique");
%>

<jsp:include page="<%=Const.PATH_HEAD_JSP%>"/>
<jsp:include page="<%=Const.PATH_MENU_JSP%>"/>
<!-- Page content -->


<div class="container">
    
    <%
if(msg ==null){
        %>
    <form action="editProduct" method="post" class="panel panel-primary form-horizontal" style="float: unset; margin: auto;">
           <h2>Formulaire pour editer  un produit </h2>
        <div class="panel-heading">
            <h3 class="panel-title">Edit Products</h3>
        </div>
        <div class="panel-body">
            <fieldset class="col-sm-10 col-lg-10 col-md-10">
                <legend>Informations  produit</legend>
                <div class="form-group">
                    <label for="nom">Nom </label>
                    <input type="text" id="nom" class="form-control" name="nom" placeholder="Nom du produit" value="<%= item.getName() %>"  required />
                </div>
                <div class="form-group">
                    <label for="categorie">Categorie </label>
                    <input type="text" id="categorie" class="form-control" name="categorie" placeholder="categorie du produit" value="<%= item.getCategory()%>" required />
                </div>
                <div class="form-group">
                    <label for="Description">description </label>
                    <input type="text" id="description" class="form-control" name="description" placeholder="description du produit" value="<%= item.getDescription()%>"  required />
                </div>
                <div class="form-group">
                    <label for="Prix">Prix </label>
                    <input type="text" id="Prix" class="form-control" name="prix" placeholder="Prix du produit" value="<%= item.getPrice()%>" required />
                </div>
                <div class="form-group">
                    <label for="Numero_de_serie">Numero De Serie </label>
                    <input type="text" id="numero_de_serie" class="form-control" name="numero_de_serie" placeholder="Numero_de_serie du produit" value="<%= item.getSerial()%>" required />
                </div>
                <div class="form-group">
                    <label for="Quantite"> Quantite </label>
                    <input type="text" id="quantite_en_stock" class="form-control" name="quantite_en_stock" placeholder="Quantite du produit"value="<%= item.getStock()%>"  required />
                </div>
                <div class="form-group">
                    <label for="Produit_actif "> Produit_actif </label>
                    <input type="text" id="produit_actif" class="form-control" name="produit_actif" placeholder="choisir 1 pour active le produit si non 0" value="<%= item.isActive()%>"  required />
                </div>
                <input type="hidden" id="produit_id" class="form-control" name="produit_id" value="<%= item.getId()%>"  required />
                
                <input type="submit" class="btn btn-default" value ="modifier" name ="action2" /> 
            </fieldset>
        </div>
    </form>
    <%}else{%> <h3><%= msg %></h3><%
}
%>

    <!-- <a href="login?action=resetPassword" style="clear: left;">Mot de passe oublie ?</a><br /> -->
</div>
<jsp:include page="<%=Const.PATH_FOOTER_JSP%>"/>