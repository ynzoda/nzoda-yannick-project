'use strict'
// array images

const numeroImages = [1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6]
// permet d'avoir a chaque numero un array qui sera positionner aleatoirement
    .map(x => [x, Math.random()])

    // permet de positionner aleatoirement et de maniere eloigner les images identiques surtout avec l'image 1
    .sort((a, b) => a[1] - b[1])

    .map(p => p[0])

const collectionImages = document.getElementsByTagName('img')
const nodeScore = document.getElementById('score')
let scoreDuJeux = 0
// boucle pour afficher chaque image
for (let counter = 0; counter < numeroImages.length; counter++) {
    collectionImages[counter].src2 = 'images/spr' + numeroImages[counter] + '.jpg'
}

const conteneurImage = document.getElementById('memory')
let incremente = 1
let premiereImageClique, deuxiemeImageClique
let timeVerificationPicture = null

conteneurImage.addEventListener('click', function (event) {
    switch (incremente) {
    // premier click
    case 1:
        if (event.target.tagName === 'IMG') {
            event.target.src = event.target.src2
            premiereImageClique = event.target
            incremente = 2
        }
        break
        // deuxieme click
    case 2:
        if (event.target.tagName === 'IMG') {
            event.target.src = event.target.src2
            deuxiemeImageClique = event.target
            incremente = 3
        }
        // donner un temps maximal pour memoriser l'image clique
        timeVerificationPicture = setTimeout(checkPicture, 500)
        break
    case 3:

        clearTimeout(timeVerificationPicture)
        checkPicture()
        break
    default:
        break
    }
})

function checkPicture () {
    startchrono()
    // si les deux images sont identiques  placer les deux elts cliquees dans une balise span et incrementer le score
    if (premiereImageClique.src2 === deuxiemeImageClique.src2) {
        premiereImageClique.replaceWith(document.createElement('span'))
        deuxiemeImageClique.replaceWith(document.createElement('span'))
        scoreDuJeux += 50
        document.getElementById('trouve').value++
        document.getElementById('score').value = scoreDuJeux
    } else {
        Math.max(0, scoreDuJeux - 40)
        premiereImageClique.src = deuxiemeImageClique.src = 'images/spr0.jpg'
    }
    // me permet de boucler jusqu'a ce que tous les images soit replacer dans le span
    incremente = 1
    nodeScore.textContent = scoreDuJeux
    if (document.getElementsByTagName('img').length === 0) {
        nodeScore.textContent += +' ' + ' ' + 'points' + '' + '' + '' + ' ' + 'bravooo vous avez Gagne ! '
    }
}
function startchrono () {
    document.getElementById('temps').value = '0:00:00:000'
    document.getElementById('temps').stepUp(1)
}

function quitter () {
    alert('vous voulez quitter le jeu')
    window.close()
}

function Start () {
    alert('Nouvelle partie')
    window.location.reload()
}

function Pause () {
    alert('Arret de la partie')
    window.stop()
}
